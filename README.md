# WebSockets

## Que es WebSockets
WebSocket es una tecnología que proporciona un canal de comunicación bidireccional y full-duplex sobre un único socket TCP. Está diseñada para ser implementada en navegadores y servidores web, pero puede utilizarse por cualquier aplicación cliente/servidor.

## Que es Socket IO
Socket.IO es una biblioteca de JavaScript para aplicaciones web en tiempo real. Permite la comunicación bidireccional en tiempo real entre clientes y servidores web. Tiene dos partes: una biblioteca del lado del cliente que se ejecuta en el navegador y una biblioteca del lado del servidor para Node.js

## Que es redis
Redis es un motor de base de datos en memoria, basado en el almacenamiento en tablas de hashes pero que opcionalmente puede ser usada como una base de datos durable o persistente

## Nest JS
Nest es un marco para crear aplicaciones de servidor Node.js eficientes y escalables. Utiliza JavaScript progresivo, está construido con TypeScript (conserva la compatibilidad con JavaScript puro) y combina elementos de OOP (Programación Orientada a Objetos), FP (Programación Funcional) y FRP (Programación Reactiva Funcional).

Por debajo, Nest utiliza Express, pero también proporciona compatibilidad con una amplia gama de otras bibliotecas (por ejemplo, Fastify). Esto permite un uso fácil de los innumerables complementos de terceros disponibles.

### Instalacion de Nest JS
Se debe Intsalar con el siguiente comando:
```
npm i -g @nestjs/cli
```
Para verificar si realmente se instalos se usa el sigueinte comando:
```
nest --version
```
### Crear un nuevo protyecto de Nest JS
```
nest new websockets
```
![imagen](imagenes/new-nest.png)

### Ejecutar Servidor Nest JS

Comprobamos que funciones correctamente con el comando:
```
npm run start
```
![imagen](imagenes/nest-run2.png)

Abrirmos en cualquier navegador con la direccion:
```
http://localhost:3000
```
![imagen](imagenes/nest-run.png)

### Crear una base de datos Redis

Se debe crear una base de datos REDIS, el cual nos ayudara a manejar la concurrencia.

Para ello usaremos docker, la cual nos ayudara a obtener el puerto y host, para poder conectarnos

![imagen](imagenes/nest-redis.png)

### Instalar dependencias WebSockets

Debemos instalar la dependecia **@nestjs/websockets** y tambien **socket.io-redis** con el siguiente comando:
```
npm i @nestjs/websockets socket.io-redis
```
* siempre instalar donde se encuentee el archivo package.json

### Configuracion Para usar WebSockest

Dentro de la carpeta **src** se debe crear 2 carpetas:
* adaptador
* websockets

![imagen](imagenes/nest-config.png)

**Nota: Todos los archivos son typescript**

El contenido de la carpeta **adaptador** contendra
* **adapter.readis.ws.ts**

Lo cual tendra el siguiente bloque de codigo, se debera cambiar el host y el puertoÑ
```javascript
import { IoAdapter } from '@nestjs/websockets';
import * as redisIoAdapter from 'socket.io-redis';

const redisAdapter = redisIoAdapter({ host: 'localhost', port: 32769 });

export class RedisIoAdapter extends IoAdapter {
  createIOServer(port: number, options?: any): any {
    const server = super.createIOServer(port, options);
    server.adapter(redisAdapter);
    return server;
  }
}

```

El contenido de la carpeta **web-socket** contendra
* Una carpeta **cliente-web-socket**
* Una carpeta **operadora-web-socket**
* **web-socket.module.ts**

![imagen](imagenes/nest-config2.png)

dentro del archivo **web-socket.module.ts** contendra lo siguiente:
```javascript
import { Module } from "@nestjs/common";
import { OperadoraGateway } from "./operadora-web-socket/operadora";
import { ClienteGateway } from "./cliente-web-socket/cliente";

@Module({
    imports: [OperadoraGateway, ClienteGateway],
    exports: [OperadoraGateway]
})

export class WebSocketModule{

}
```

La carpeta **cliente-web-socket** contendra un archivo llamado **cliente.ts** con el contenido siguiente:
```javascript
import {
    WebSocketGateway,
    WebSocketServer,
    SubscribeMessage,
    WsResponse,
    OnGatewayInit,
    OnGatewayConnection,
    OnGatewayDisconnect
} from '@nestjs/websockets';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
const io = require('socket.io-client');

@WebSocketGateway(3002, { namespace: '/cliente' })

export class ClienteGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {

    misUsuarios: any[] = []

    handleDisconnect(client: any) {
        console.log('disconnect', client.id);
    }
    handleConnection(client: any, ...args: any[]) {
        console.log('cliente -> conexion de aperadora', client.id, args);
    }
    afterInit(server: any) {
        console.log('Init cliente');
    }

    @SubscribeMessage('ingresoCliente') //nombre del evento
    nuevoUsuario(client, data): Observable<WsResponse<number>> {
        const miUsuario = { id: client.id, nickName: data }
        let salida:any = ''
        let existeElUsuario = this.misUsuarios.find(usuario => {
            return usuario.id == miUsuario.id
        })
        if (existeElUsuario) {
            existeElUsuario.nickName = miUsuario.nickName
            client.broadcast.emit('cambioDeNick', existeElUsuario)
            salida = 'cambio a este nombre: ' + existeElUsuario.nickName
        } else {
            this.misUsuarios.push(miUsuario)
            client.broadcast.emit('nuevoClienteEnBroadcast', miUsuario)
            salida = 'Hola bienvenido' + miUsuario.nickName
        }
        return salida; // la peticion 
    }


    @SubscribeMessage('enviandoMensaje') //nombre del evento
    nuevoMensaje(client, data): Observable<WsResponse<number>> {
        client.broadcast.emit('enviandoMensajeBroadcast', data)
        return data; // la peticion 
    }

}
```
Para poder extableces la conexion se usa:
```javascript
@WebSocketGateway(3002, { namespace: '/cliente' })
```
* Donde el namespace es el nombre de la conexion.

Para el uso de WebSockets se usa  ```@SubscribeMessage()``` en cual contendra el nombre del evento a escuchar, mediante ``` broadcast.emit``` se podra enviar la respuesta a todos los clientes conectados

De parte de la operadora es lo mismo.

La carpeta **operadora-web-socket** contendra un archivo llamado **operadora.ts** con el contenido siguiente:
```javascript
import {
    WebSocketGateway,
    WebSocketServer,
    SubscribeMessage,
    WsResponse,
    OnGatewayInit,
    OnGatewayConnection,
    OnGatewayDisconnect
} from '@nestjs/websockets';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
const io = require('socket.io-client');


@WebSocketGateway(3002,{namespace: '/operadora'})

export class OperadoraGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect{
    
    rooms:any[] = []

    afterInit(server: any) {
        console.log('Init Operadora-escucha');
    }

    handleConnection(client: any, ...args: any[]) {
        console.log('Operadora -> conexion de cliente', client.id, args);
    }

    handleDisconnect(client: any) {
        console.log('disconnect', client.id);
    }

    
    @SubscribeMessage('ingresoCliente') //nombre del evento
	nuevoUsuario(client, data): Observable<WsResponse<number>> {       
        client.broadcast.emit('nuevoClienteEnBroadcast',data)
		return data; // la peticion 
    }

    
    @SubscribeMessage('enviandoMensaje') //nombre del evento
	nuevoMensaje(client, data): Observable<WsResponse<number>> {
        client.broadcast.emit('enviandoMensajeBroadcast',data)
		return data; // la peticion 
    }



    @SubscribeMessage('peticionUnirseAlRoom') //nombre del evento
	unirseAlRoom(client, objetoRoom): Observable<WsResponse<number>> {
        this.rooms.push(objetoRoom.room)
        client.join(objetoRoom.room) // buscar y unirse a una sala e caht
        client.broadcast.emit('respuestaUnionRoom',objetoRoom)
		return objetoRoom; // la peticion 
    }

    @SubscribeMessage('peticionMensajeAlRoom') //nombre del evento
	mesnajeAlRoom(client, objetoRoom): Observable<WsResponse<number>> {
        client.to(objetoRoom.room).emit('respuestaMensajeRoom',objetoRoom)
		return objetoRoom; // la peticion 
    }

    @SubscribeMessage('peticionSalirRoom') //nombre del evento
	salirDelRoom(client, room): Observable<WsResponse<number>> {
        client.leave(room) // string
        client.to(room).emit('respuestaSalioRoom',room)
        client.broadcast.emit('respuestaSalirDelRoomBroadcast',room)
		return room; // la peticion 
    }    
}
```

Lo que cambia es el namespace
```javascript
@WebSocketGateway(3002, { namespace: '/operadora' })
```

![imagen](imagenes/nest-config3.png)

Dentro del archivo **main.ts**
se debe contener lo siguiente:
```javascript
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { RedisIoAdapter } from 'adaptador/adapter.readis.ws';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useWebSocketAdapter(new RedisIoAdapter(app));
  app.useStaticAssets(__dirname + '/../public')
  await app.listen(3000);

}
bootstrap();
```
Lo cual ayiudar a conectar con redis, ademas, se creara una carpeta public, donde contendremos archivos publicos con los cuales podremos comunicarnos mediante web-sockets

![imagen](imagenes/nest-config4.png)

Dentro de esa carpeta se los siguientes archivos:
* index.html
* cliente.html
* operadora.html
* finalmente la libreria que ayudara a la conexion **socket.io.js**

![imagen](imagenes/nest-config5.png)

Contenido del archivo **index.html**
```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <script src="socket.io.js"></script>



    <title>Document</title>
</head>

<body>
    <h1>Hola</h1>

    <script>
        const socketCliente = io('http://localhost:3002/cliente');
        const socketOperadora = io('http://localhost:3002/operadora');

        socketCliente.on('nuevoClienteEnBroadcast', (miNombre) => {
            console.log('se unio desde cliente' ,miNombre)
        });

        socketOperadora.on('nuevoClienteEnBroadcast', (miNombre) => {
            console.log('se unio desde operadora',miNombre)
        });

    </script>
</body>

</html>

```

Contenido del archivo **cliente.html**
```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <script src="socket.io.js"></script>



    <title>Document</title>
</head>

<body>
    <h1>Hola</h1>

    

    <script>
        const socketCliente = io('http://localhost:3002/cliente');
    
        
        let nick = ''

        socketCliente.on('connect', () => {
            console.log('Socket Conectado Correctamente Cliente')
        });

        
        socketCliente.on('nuevoClienteEnBroadcast', (usuario) => {
            console.log('se unio',usuario.nickName)
        });

        
        socketCliente.on('cambioDeNick', (usuario) => {
            console.log('cambio a este nombre: ',usuario.nickName)
        });


        function loginCliente(nombre) {
            nick = nombre
            socketCliente.emit('ingresoCliente', nick, (data) => {
                console.log(data)
            })
        }

        
        socketCliente.on('enviandoMensajeBroadcast', (mensajeEnBroadcast) => {
            console.log(`${mensajeEnBroadcast.fecha} ${mensajeEnBroadcast.nick}: ${mensajeEnBroadcast.mensaje}`)
        });


        function mensajeCliente(miMensaje) {
            socketCliente.emit('enviandoMensaje',{fecha: new Date().toUTCString(), nick:nick, mensaje:miMensaje}, (data) => {
                console.log(data.mensaje)
            })
        }



        socketCliente.on('disconnect', () => {
            console.log('Se desconecto el Socket cliente')
        })

    </script>
</body>

</html>

```

Contenido del archivo **operadora.html**
```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <script src="socket.io.js"></script>



    <title>Document</title>
</head>

<body>
    <h1>Hola</h1>

    <script>
        const socketOperadora = io('http://localhost:3002/operadora');

        let nick = ''

        socketOperadora.on('connect', () => {
            console.log('Socket Conectado Correctamente operadora')
        });

        
        socketOperadora.on('nuevoClienteEnBroadcast', (miNombre) => {
            console.log('se unio',miNombre)
        });


        function loginOperadora(nombre) {
            nick = nombre
            socketOperadora.emit('ingresoCliente', nick, (data) => {
                console.log('Hola bienvenido', data)
            })
        }

        
        socketOperadora.on('enviandoMensajeBroadcast', (mensajeEnBroadcast) => {
            console.log(`${mensajeEnBroadcast.fecha} ${mensajeEnBroadcast.nick}: ${mensajeEnBroadcast.mensaje}`)
        });


        function mensajeOperadora(miMensaje) {
            socketOperadora.emit('enviandoMensaje',{fecha: new Date().toUTCString(), nick:nick, mensaje:miMensaje}, (data) => {
                console.log(data.mensaje)
            })
        }


        ////////////// rooooms

        
        socketOperadora.on('respuestaUnionRoom', (respuestDeUnionAlRomm) => {
            console.log('se unio al room',respuestDeUnionAlRomm.room)
        });

        
        socketOperadora.on('respuestaMensajeRoom', (respuestMensaje) => {
            console.log('Del room ',respuestMensaje.room,' te envia esto: ',respuestMensaje.mensaje)
        });

        
        socketOperadora.on('respuestaSalioRoom', (respuestDeSalioRoom) => {
            console.log('Saliste del room:',respuestDeSalioRoom)
        });

        
        socketOperadora.on('respuestaSalirDelRoomBroadcast', (respuestDeSalioRoom) => {
            console.log('Le dieron Ban:',respuestDeSalioRoom)
        });


        function entrarAlRoom(objetoRoom) {
            socketOperadora.emit('peticionUnirseAlRoom',objetoRoom, (respuestaPeticionRoom) => {
                console.log('entraste al room: ',respuestaPeticionRoom.room)
            })
        }

        function mensajeAlRoom(objetoRoom) {
            socketOperadora.emit('peticionMensajeAlRoom',objetoRoom, (respuestaMensajePeticionRoom) => {
                console.log('Del:',respuestaMensajePeticionRoom.room,' te envia esto ',respuestaMensajePeticionRoom.mensaje)
            })
        }

         function salirRoom(room) {
            socketOperadora.emit('peticionSalirRoom',room, (respuestaMensajePeticionRoom) => {
                console.log('saliste bro del room: ',respuestaMensajePeticionRoom)
            })
        }




        socketOperadora.on('disconnect', () => {
            console.log('Se desconecto el Socket operadora')
        })


    </script>
</body>

</html>

```

Nos dirigimos a index.html

![imagen](imagenes/nest-run3.png)


